import React from 'react';
import { CaretRight, CaretLeft } from 'phosphor-react';
import Avatar from '../bruh.jpg';

export default function Footer() {
    return (
        <>
            <footer className="thanks visible">
                <div className='gli '>
                    <div className='flex p-4 sm:justify-around justify-center flex-col sm:flex-row '>
                        <div className='flex flex-col font-bold text-center'>
                            <h1 className='text-2xl'>Made with ❤️</h1>
                            {/* <h1>Front End Enthusiast</h1> */}
                        </div>
                        <div className='flex flex-row p-2 space-x-2 justify-center'>
                            <a className="hover-underline-animation p-0" href="https://github.com/primalkz">Github</a>,
                            <a className="hover-underline-animation p-0" href="https://discord.com/users/565816382231412766">Discord</a>,
                            <a className="hover-underline-animation p-0" href="https://www.linkedin.com/in/gaurang-jha-4218b4254/">LinkedIn</a>
                        </div>
                        
                    </div>
                </div>
            </footer>
        </>
    );
}
