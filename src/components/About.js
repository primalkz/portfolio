import React from 'react';
import '../App.css';

function About() {
  return (
    <section id='about'>
      <div class="ab-container px-6 py-6 visible h-full sm:min-h-auto">
        <div class="sm:text-3xl sm:py-16">
          <h1 class="text-2xl sm:text-4xl px-6 font-bold sm:px-32">About</h1>
          <p class="sm:px-32 px-6 py-6 sm:text-xl ">
            Hello! I am a 20-year-old Geek and Front End Enthusiast, currently pursuing a degree in  <span>Computer Engineering</span>.
            I believe I am <a href="https://en.wiktionary.org/wiki/wonk">wonk</a> when it comes to Linux and tech.
            <br />
            <br />
            I am not a professional programmer. However, I am able to write simple programmes in C, C++, Rust, Bash, and JS, and I use ReactJS as my Front-End Framework for developing websites.
            
            <br />
            <br />
            I am also learning video editing, currently I am learning after effects.
          </p>
        </div>
      </div>
    </section>
  );
}

export default About;
