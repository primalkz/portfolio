
import React from 'react';
import '../App.css';
// import { Icon } from '@iconify/react';

function Projects() {
    return (
        <section className='h-full sm:h-full'>

            <div id="projects" className=" -skew-y-3 mb-14 mt-6 dark:bg-[#060607] bg-white-300 visible">
                <div className="skew-y-3  py-12 ">
                    <div className='flex justify-center m-2 p-4 flex-col projects'>
                        <h1 className="sm:text-4xl flex font-bold justify-left sm:ml-44 p-5 mb-2 text-2xl">Projects</h1>
                        <a href="https://github.com/primalkz/coderunner-rs" className='p-4 m-2'>
                            <div className="flex justify-around ">
                                <h1 className='sm:text-3xl text-xl 2xl:text-4xl text-bold'>
                                    01/
                                </h1>
                                <p className='sm:text-xl font-thin sm:max-w-[500px] max-w-auto px-4 sm:p-0'>
                                    <span>coderunner-rs</span> is a code compiler cum runner program written in rust 
                                    which runs the code based on the extension of file.
                                </p>
                            </div>
                        </a>
                        <a href="https://github.com/primalkz/suckless" className='p-4 m-2 '>
                            <div className="flex justify-around ">
                                <h1 className='sm:text-3xl text-xl 2xl:text-4xl text-bold'>
                                    02/
                                </h1>
                                <p className='sm:text-xl font-thin sm:max-w-[500px] max-w-auto px-4 sm:p-0'>
                                    <span>DWM</span> is a beautiful yet minimal window manager written in C by suckless devs, which I worked on to make it little more functional with many features.
                                </p>
                            </div>
                        </a>
                        <a href="https://github.com/primalkz/CestaWebpage" className='p-4 m-2'>
                            <div className="flex justify-around ">
                                <h1 className='sm:text-3xl text-xl 2xl:text-4xl text-bold'>
                                    03/
                                </h1>
                                <p className='sm:text-xl font-thin sm:max-w-[500px] max-w-auto px-4 sm:p-0'>
                                    My college's coding club website which I made in a team.
                                </p>
                            </div>
                        </a>
                        <a href="https://portfolio-new-two-psi.vercel.app/" className='p-4 m-2'>
                            <div className="flex justify-around ">
                                <h1 className='sm:text-3xl text-xl 2xl:text-4xl text-bold'>
                                    04/
                                </h1>
                                <p className='sm:text-xl font-thin sm:max-w-[500px] max-w-auto px-4 sm:p-0'>
                                    My portfolio website I made using reactjs and tailwind css. I made it minimal and fast.
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Projects;